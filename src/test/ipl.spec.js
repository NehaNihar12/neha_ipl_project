const assert = require ('assert');
const {expect} =  require("chai");
const ipl = require('../server/ipl.js')

describe('getmatchesPerYear()',()=>{
    it('should return number of matches per year',()=>{
        const sampleMatches = [
            {'season':2015},
            {'season':2015},
            {'season':2014},
            {'season':2012}
    
        ]
        const expectedResult = {
            '2012':1,
            '2014':1,
            '2015':2
        }
        const actualResult = ipl.getMatchesPerYear(sampleMatches)
        expect(actualResult).to.deep.equal(expectedResult);
    });
    it('should return empty object for empty array',()=>{
        const actualResult = ipl.getMatchesPerYear([]);
        expect(actualResult).to.deep.equal({});
    })
    it('should return empty object for no argument',()=>{
        const actualResult = ipl.getMatchesPerYear();
        expect(actualResult).to.deep.equal({});
    })    
})

describe('getMatchesWonByTeamsPerYear()',()=>{
    it('should return number of matches won by teams per year',()=>{
        const sampleMatches = [
            {
                'season':2005,
                'winner':'CSK'
            },
            {
                'season':2005,
                'winner':'CSK'
            },
            {
                'season':2007,
                'winner':'KKR'
            },
            {
                'season':2008,
                'winner':'DD'
            }
    
        ]
        const expectedResult = {
            'CSK':{
                '2005':2,
                '2007':0,
                '2008':0
            },
            'KKR':{
                '2005':0,
                '2007':1,
                '2008':0
            },
            'DD':{
                '2005':0,
                '2007':0,
                '2008':1
            }
        }
        const actualResult = ipl.getMatchesWonByTeamsPerYear(sampleMatches)
        expect(actualResult).to.deep.equal(expectedResult);
    });
    it('should return empty object for empty array argument',()=>{
        const actualResult = ipl.getMatchesWonByTeamsPerYear([]);
        expect(actualResult).to.deep.equal({});
    })
    it('should return empty object for no argument',()=>{
        const actualResult = ipl.getMatchesWonByTeamsPerYear();
        expect(actualResult).to.deep.equal({});
    })    
})

describe('getExtraRunsConcededPerTeamInGivenYear()',()=>{
    it('should return number of matches won by teams per year',()=>{
        const sampleMatches = [
            {
                'id':1,
                'season':2017
            },
            {
                'id':2,
                'season':2005
            },
            {
                'id':4,
                'season':2005
            },
            {
                'id':5,
                'season':2017
            },
            {
                'id':129,
                'season':2017
            },
            {
                'id':9,
                'season':2017
            },
            {
                'id':12,
                'season':2017
            },
            {
                'id':60,
                'season':2017
            }
        ]

        const sampleDeliveries = [
            {
                'match_id':1,
                'bowling_team':'RC',
                'extra_runs':1
                
            },
            {
                'match_id':2,
                'bowling_team':'RPS',
                'extra_runs':0
            },
            {
                'match_id':4,
                'bowling_team':'RPS',
                'extra_runs':4
            },
            {
                'match_id':5,
                'bowling_team':'RC',
                'extra_runs':1
            },
            {
                'match_id':129,
                'bowling_team':'DD',
                'extra_runs':2
            },
            {
                'match_id':9,
                'bowling_team':'DD',
                'extra_runs':0
            },
            {
                'match_id':12,
                'bowling_team':'KKR',
                'extra_runs':1
            },
            {
                'match_id':60,
                'bowling_team':'KKR',
                'extra_runs':1
            }
        ]
        const sampleYear = 2017;

        const expectedResult = {
            'RC':2,
            'DD':2,
            'KKR':2
        }
        const actualResult = ipl.getExtraRunsConcededPerTeamInGivenYear(sampleDeliveries,sampleMatches,sampleYear)
        expect(actualResult).to.deep.equal(expectedResult);
    });
    it('should return empty object for empty array argument',()=>{
        const actualResult = ipl.getExtraRunsConcededPerTeamInGivenYear([]);
        expect(actualResult).to.deep.equal({});
    })
    it('should return empty object for no argument',()=>{
        const actualResult = ipl.getExtraRunsConcededPerTeamInGivenYear();
        expect(actualResult).to.deep.equal({});
    })    
    
});

describe('getTop10EconomicalBowlersInGivenYear()',()=>{
    it('should return number of matches won by teams per year',()=>{
        const sampleMatches = [
            {
                'id':1,
                'season':2017
            },
            {
                'id':2,
                'season':2005
            },
            {
                'id':4,
                'season':2005
            },
            {
                'id':5,
                'season':2017
            },
            {
                'id':129,
                'season':2017
            },
            {
                'id':9,
                'season':2017
            },
            {
                'id':12,
                'season':2017
            },
            {
                'id':60,
                'season':2017
            }
        ]

        const sampleDeliveries = [
            {
                'match_id':1,
                'bowler':'a1',
                'total_runs':1
                
            },
            {
                'match_id':2,
                'bowler':'a1',
                'total_runs':2
            },
            {
                'match_id':4,
                'bowler':'a1',
                'total_runs':3
            },
            {
                'match_id':5,
                'bowler':'a1',
                'total_runs':9
            },
            {
                'match_id':129,
                'bowler':'a1',
                'total_runs':2
            },
            {
                'match_id':9,
                'bowler':'b1',
                'total_runs':6
            },
            {
                'match_id':12,
                'bowler':'b2',
                'total_runs':2
            },
            {
                'match_id':60,
                'bowler':'b3',
                'total_runs':5
            }
        ]
        const sampleYear = 2017;

        const expectedResult = {
            'b2':12,
            'a1':24,
            'b3':30,
            'b1':36
        }
        const actualResult = ipl.getTop10EconomicalBowlersInGivenYear(sampleDeliveries,sampleMatches,sampleYear)
        expect(actualResult).to.deep.equal(expectedResult);
    });
    it('should return empty object for empty array argument',()=>{
        const actualResult = ipl.getTop10EconomicalBowlersInGivenYear([]);
        expect(actualResult).to.deep.equal({});
    })
    it('should return empty object for no argument',()=>{
        const actualResult = ipl.getTop10EconomicalBowlersInGivenYear();
        expect(actualResult).to.deep.equal({});
    })    
    
});