const getMatchesPerYear = function (matches=[]) {
    return matches.map((match) => match.season)
        .reduce((acc, currVal) => {
            if (!acc[currVal]) {
                acc[currVal] = 1
            } else {
                acc[currVal] += 1
            }
            return acc;
        }, {})
}


const getMatchesWonByTeamsPerYear = function (matches=[]) {
    
    let years = Array.from(new Set(matches.map((obj) => obj.season))).map((year) => [year, 0])

    let matchesWonByTeamsPerYear = {};

    matches.forEach((match) => {
        let winner = match.winner;
        let season = match.season;
        let team1 = match.team1
        let team2 = match.team2;
        if(winner){
        if (!matchesWonByTeamsPerYear.hasOwnProperty(team1)) {

            matchesWonByTeamsPerYear[team1] = Object.fromEntries(years);
  
        }
        if (!matchesWonByTeamsPerYear.hasOwnProperty(team2)) {
  
            matchesWonByTeamsPerYear[team2] = Object.fromEntries(years);
  
        }
        if (!matchesWonByTeamsPerYear.hasOwnProperty(winner)) {

            matchesWonByTeamsPerYear[winner] = Object.fromEntries(years);
            matchesWonByTeamsPerYear[winner][season] += 1

        } else {

            matchesWonByTeamsPerYear[winner][season] += 1

        }}
    });
    return matchesWonByTeamsPerYear
}




const getExtraRunsConcededPerTeamInGivenYear = function (deliveries=[], matches=[], year) {

    //matchId and season key-value pair
    let matchIdSeasonMap = {};
    matches.forEach((match) => {
        matchIdSeasonMap[match.id] = match.season;
    })
    let extraRunsConcededByTeams = {}
    deliveries.forEach((delivery) => {
        let matchId = delivery["match_id"];
        if (matchIdSeasonMap[matchId] === year) {
            let bowlingTeam = delivery.bowling_team;
            let extraRuns = delivery.extra_runs;

            if (!extraRunsConcededByTeams.hasOwnProperty(bowlingTeam)) {
                extraRunsConcededByTeams[bowlingTeam] = extraRuns;
            } else {
                extraRunsConcededByTeams[bowlingTeam] += extraRuns
            }
        }
    })
    return extraRunsConcededByTeams
}


const getTop10EconomicalBowlersInGivenYear = function (deliveries=[], matches=[], year) {

    //matchId and season key-value pair
    let matchIdSeasonMap = {};
    matches.forEach((match)=> {
        matchIdSeasonMap[match.id] = match.season;
    })
    let bowlersHistory = {}
    deliveries.forEach((delivery) => {
        let matchId = delivery["match_id"];
        if (matchIdSeasonMap[matchId] === year) {
            let bowler = delivery.bowler;
            let runsConceded = delivery.total_runs
            if (!bowlersHistory.hasOwnProperty(bowler)) {
                bowlersHistory[bowler] = {};
                bowlersHistory[bowler]['runsConceded'] = runsConceded;
                bowlersHistory[bowler]['ballsPlayed'] = 1;
            } else {
                bowlersHistory[bowler]['runsConceded'] += runsConceded;
                bowlersHistory[bowler]['ballsPlayed'] += 1
            }
        }
    });  
    for (bowler in bowlersHistory) {
        let overs = (bowlersHistory[bowler].ballsPlayed) / 6;
        if (overs > 0) {
            let economy = Number(((bowlersHistory[bowler].runsConceded) / overs).toFixed(2))
            bowlersHistory[bowler] = economy
        } else {
            delete bowlersHistory[bowler]
        }
    }
    let top10EconomicalBowlers = Object.entries(bowlersHistory)
        .sort((a, b) => a[1] - b[1])
        .slice(0,10)

    return Object.fromEntries(top10EconomicalBowlers);
}


const getNumberofTimesTossWinnerWins = function (matches) {
    let teams = Array.from(new Set(matches.map((match) => match.team1))).map(team => [team, 0]);
    let numberofTimesTossWinnerWins = Object.fromEntries(teams);
    matches.forEach((match) => {
        let winner = match.winner;
        if (match.toss_winner === winner) {
            numberofTimesTossWinnerWins[winner] += 1;
        }
    })
    return numberofTimesTossWinnerWins
}


const getPlayerOfEachSeason = function (matches) {

    let playerOfEachSeson = {}
    matches.forEach((match) => {
        let player = match.player_of_match;
        let season = match.season

        if (!playerOfEachSeson.hasOwnProperty(season)) {
            playerOfEachSeson[season] = {};
        }
        if (!playerOfEachSeson[season].hasOwnProperty(player)) {
            playerOfEachSeson[season][player] = 1;
        } else {
            playerOfEachSeson[season][player] += 1;
        }


    })
    playerOfEachSeson = Object.entries(playerOfEachSeson)

    let topPlayersOfEachSeason = {};

    playerOfEachSeson.forEach((season) => {
        let manOfSeason = [];
        let playersOfSeason = Object.entries(season[1])
            .sort((player1, player2) => player2[1] - player1[1])
        let max = playersOfSeason[0][1];
        for (let i = 0; i < playersOfSeason.length; i++) {
            if (playersOfSeason[i][1] !== max) {
                break;
            } else {
                manOfSeason.push(playersOfSeason[i][0])
            }
        }
        topPlayersOfEachSeason[season[0]] = manOfSeason
    })

    return topPlayersOfEachSeason
}


const getStrikeRateOfBatsmanForEachSeason = function (deliveries, matches) {
    let matchHistory = matches
    let strikeRateOfBatsmanForEachSeason = {};
    let year;

    deliveries.forEach((delivery) => {
        let i = delivery.match_id - 1;
        if (i >= 0) {
            year = matchHistory[i].season;
        }
        let batsman = delivery.batsman;

        if (!strikeRateOfBatsmanForEachSeason.hasOwnProperty(year)) {
            strikeRateOfBatsmanForEachSeason[year] = {};
        }
        if (!strikeRateOfBatsmanForEachSeason[year].hasOwnProperty(batsman)) {
            strikeRateOfBatsmanForEachSeason[year][batsman] = {};
        }
        if (!strikeRateOfBatsmanForEachSeason[year][batsman].hasOwnProperty('batsmanRuns')) {
            strikeRateOfBatsmanForEachSeason[year][batsman].batsmanRuns = delivery.batsman_runs
        } else {
            strikeRateOfBatsmanForEachSeason[year][batsman].batsmanRuns += delivery.batsman_runs
        }
        if (!strikeRateOfBatsmanForEachSeason[year][batsman].hasOwnProperty('totalBalls')) {
            strikeRateOfBatsmanForEachSeason[year][batsman].totalBalls = 1
        } else {
            strikeRateOfBatsmanForEachSeason[year][batsman].totalBalls += 1
        }

    })

    for (let season in strikeRateOfBatsmanForEachSeason) {
        for (let batsman in strikeRateOfBatsmanForEachSeason[season]) {
            let totalBalls = strikeRateOfBatsmanForEachSeason[season][batsman]['totalBalls'];
            let totalRuns = strikeRateOfBatsmanForEachSeason[season][batsman]['batsmanRuns']
            if (totalBalls > 0) {
                let strikeRateOfBatsman = (totalRuns / totalBalls) * 100;
                strikeRateOfBatsmanForEachSeason[season][batsman] = strikeRateOfBatsman;
            }
        }

    }
    return strikeRateOfBatsmanForEachSeason;
}


const getHighestNumberOfTimesAplayerHasBeenDismissedByAnotherPlayer = function (deliveries) {
    let noOfTimesAPlayerWasDismissed = {}
    let dismissalType = []
    deliveries.forEach((obj) => {
        let playerDismissed = obj.player_dismissed;
        dismissalType = obj.dismissal_kind;
        if (playerDismissed && (dismissalType === 'bowled' || dismissalType === 'caught and bowled' || dismissalType === 'run out' || dismissalType === 'stumped')) {
            if (!noOfTimesAPlayerWasDismissed.hasOwnProperty(playerDismissed)) {
                noOfTimesAPlayerWasDismissed[playerDismissed] = 1
            } else {
                noOfTimesAPlayerWasDismissed[playerDismissed] += 1
            }
        }
    })
    let highestNoOfTimesAPlayerWasDismissed = Object.entries(noOfTimesAPlayerWasDismissed).sort((player1, player2) => player2[1] - player1[1])
    return (highestNoOfTimesAPlayerWasDismissed[0][1])
}


const getMostEconomicalBowlerInSuperOvers = function (deliveries) {
    let superOvers = deliveries.filter(obj => obj.is_super_over)

    let bowlersHistoryInSuperOvers = {}
    superOvers.forEach((match) => {
        let bowler = match.bowler;
        let runsConceded = match.total_runs
        if (!bowlersHistoryInSuperOvers.hasOwnProperty(bowler)) {
            bowlersHistoryInSuperOvers[bowler] = {};
            bowlersHistoryInSuperOvers[bowler]['runsConceded'] = runsConceded;
            bowlersHistoryInSuperOvers[bowler]['ballsPlayed'] = match.ball;
        } else {
            bowlersHistoryInSuperOvers[bowler]['runsConceded'] += runsConceded;
            bowlersHistoryInSuperOvers[bowler]['ballsPlayed'] += 1
        }
    })
    for (bowler in bowlersHistoryInSuperOvers) {
        let overs = (bowlersHistoryInSuperOvers[bowler].ballsPlayed) / 6;
        if (overs > 0) {
            let economy = (bowlersHistoryInSuperOvers[bowler].runsConceded) / overs
            bowlersHistoryInSuperOvers[bowler] = economy
        } else {
            delete bowlersHistoryInSuperOvers[bowler]
        }
    }
    let mostEconomicalBowlersInSuperOvers = Object.entries(bowlersHistoryInSuperOvers)
        .sort((a, b) => a[1] - b[1])
        .slice(0,1)[0]

    return mostEconomicalBowlersInSuperOvers[0];
}


module.exports = {
    getMatchesPerYear,
    getMatchesWonByTeamsPerYear,
    getExtraRunsConcededPerTeamInGivenYear,
    getTop10EconomicalBowlersInGivenYear,
    getNumberofTimesTossWinnerWins,
    getPlayerOfEachSeason,
    getStrikeRateOfBatsmanForEachSeason,
    getHighestNumberOfTimesAplayerHasBeenDismissedByAnotherPlayer,
    getMostEconomicalBowlerInSuperOvers
}