const converter = require('json-2-csv');
const fs = require('fs')
const path = require("path");
const ipl = require('./ipl.js')

const MATCHES_CSV_PATH = `${__dirname}/../data/matches.csv`;
const DELIVERIES_CSV_PATH = `${__dirname}/../data/deliveries.csv`;

//Reading deliveries csv file and converting it to oject 
let deliveries = [];
const csv = fs.readFileSync(DELIVERIES_CSV_PATH)
let arrOfCsvData = csv.toString().split("\n");
let headers = arrOfCsvData[0].replace(/\r?\n|\r/g, "").split(",")

for (let i = 1; i < arrOfCsvData.length; i++) {
    let rowData = arrOfCsvData[i].replace(/\r?\n|\r/g, "").split(',')
        .reduce(function (acc, currVal, currIndex) {
            let header = headers[currIndex]
            if (/\d+/g.test(currVal)) {
                acc[header] = Number(currVal)
            } else {
                acc[header] = currVal;
            }
            return acc;
        }, {});
    deliveries.push(rowData)
}


//reading matches.csv file and converting in JSON format
fs.readFile(MATCHES_CSV_PATH, 'utf8', (err, stringifiedCsvData) => {
    if (err) {
        console.error(err)
    }
    converter.csv2json(stringifiedCsvData, function (err, matches) {

        //1.
        let matchesPerYear = ipl.getMatchesPerYear(matches)
        console.log(matchesPerYear)
        writeFile(
            path.resolve(__dirname, "../public/output/matchesPerYear.json"),
            JSON.stringify(matchesPerYear, null, 2)
        )

        //2.
        let matchesWonByTeamsPerYear = ipl.getMatchesWonByTeamsPerYear(matches)
        console.log(matchesWonByTeamsPerYear)
        writeFile(
            path.resolve(__dirname, "../public/output/matchesWonByTeamsPerYear.json"),
            JSON.stringify(matchesWonByTeamsPerYear, null, 2)
        )

        //3.
        let extraRunsConcededPerTeamInYear2016 = ipl.getExtraRunsConcededPerTeamInGivenYear(deliveries, matches, 2016)
        console.log(extraRunsConcededPerTeamInYear2016);
        writeFile(
            path.resolve(__dirname, "../public/output/extraRunsConcededPerTeamIn2016.json"),
            JSON.stringify(extraRunsConcededPerTeamInYear2016, null, 2)
        )

        //4.
        let top10EconomicalBowlersIn2015 = ipl.getTop10EconomicalBowlersInGivenYear(deliveries, matches, 2015)
        console.log(top10EconomicalBowlersIn2015);
        writeFile(
            path.resolve(__dirname, "../public/output/top10EconomicalBowlersIn2015.json"),
            JSON.stringify(top10EconomicalBowlersIn2015, null, 2)
        )

        //5.
        let numberofTimesTossWinnerWins = ipl.getNumberofTimesTossWinnerWins(matches)
        console.log(numberofTimesTossWinnerWins);
        writeFile(
            path.resolve(__dirname, "../public/output/numberofTimesTossWinnerWins.json"),
            JSON.stringify(numberofTimesTossWinnerWins, null, 2)
        )

        //6.
        let playerOfMatchOfEachSeason = ipl.getPlayerOfEachSeason(matches)
        console.log(playerOfMatchOfEachSeason);
        writeFile(
            path.resolve(__dirname, "../public/output/playerOfMatchOfEachSeason.json"),
            JSON.stringify(playerOfMatchOfEachSeason, null, 2)
        )

        //7. 
        let strikeRateOfBatsmanForEachSeason = ipl.getStrikeRateOfBatsmanForEachSeason(deliveries, matches)
        console.log(strikeRateOfBatsmanForEachSeason);
        writeFile(
            path.resolve(__dirname, "../public/output/strikeRateOfBatsmanForEachSeason.json"),
            JSON.stringify(strikeRateOfBatsmanForEachSeason, null, 2)
        )

    });
});

//8.
let highestNoOfTimesAPlayerWasDismissedByAnotherPlayer = ipl.getHighestNumberOfTimesAplayerHasBeenDismissedByAnotherPlayer(deliveries)
console.log(highestNoOfTimesAPlayerWasDismissedByAnotherPlayer);
writeFile(
    path.resolve(__dirname, "../public/output/highestNoOfTimesAPlayerWasDismissedByAnotherPlayer.json"),
    JSON.stringify(highestNoOfTimesAPlayerWasDismissedByAnotherPlayer, null, 2)
)

//9.
let mostEconomicalBowlerInSuperOvers = ipl.getMostEconomicalBowlerInSuperOvers(deliveries)
console.log(mostEconomicalBowlerInSuperOvers);
writeFile(
    path.resolve(__dirname, "../public/output/mostEconomicalBowlerInSuperOvers.json"),
    JSON.stringify(mostEconomicalBowlerInSuperOvers, null, 2)
)

//function to write file
function writeFile(path, data) {
    fs.writeFile(path, data, function (err) {
        if (err) {
            console.log(err);
        }
    })
}
