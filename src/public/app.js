fetch('./output/matchesPerYear.json')
    .then((response) => response.json())
    .then((data) => generateGraph1(data))

function generateGraph1(matchesPerYear) {
    Highcharts.chart('container1', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches per Year'
        },
        subtitle:{
            text: 'IPL'
        },
        xAxis: {
            categories: Object.keys(matchesPerYear)
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No Of matches Played'
            }
        },
        plotOptions: {
            column: {
              dataLabels: {
                enabled: true
              }
            }
          },
        series: [{
            name: 'matches',
            data: Object.values(matchesPerYear)

        }]
    });
}

fetch('./output/matchesWonByTeamsPerYear.json')
    .then((response) => response.json())
    .then((data) => generateGraph2(data))

function generateGraph2(matchesWonByTeamsPerYear) {
    let seriesData = [];
    let xAxisYearValues = Object.keys(Object.entries(matchesWonByTeamsPerYear)[0][1])
    let valuesObj = Object.values(matchesWonByTeamsPerYear);
    let teams = Object.keys(matchesWonByTeamsPerYear);
    let index=0
    for (let obj of valuesObj) {
        let seriesObj = {}
        seriesObj.name = teams[index++]
        seriesObj.data = Object.values(obj);
        seriesData.push(seriesObj)
        
    }

    Highcharts.chart('container2', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Matches won by teams per Year'
        },
        subtitle:{
            text: 'IPL'
        },
        xAxis: {
            categories: xAxisYearValues
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No Of matches won'
            }
        },
        series: seriesData
    });
}

fetch('./output/extraRunsConcededPerTeamIn2016.json')
    .then((response) => response.json())
    .then((data) => generateGraph3(data))
function generateGraph3(extraRunsConceededPerTeam) {
    Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Extra Runs Conceeded Per Team in 2016'
        },
        subtitle:{
            text: 'IPL'
        },
        xAxis: {
            categories: Object.keys(extraRunsConceededPerTeam)
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Extra runs conceeded'
            }
        },
        plotOptions: {
            column: {
              dataLabels: {
                enabled: true
              }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -100,
            y: 10,
            floating: true,
            borderWidth: 1,
            backgroundColor:
              Highcharts.defaultOptions.legend.backgroundColor || '#FFFF1F',
            shadow: true
        },  
        series: [{
            name: 'Year 2016',
            data: Object.values(extraRunsConceededPerTeam)

        }]
    });
}

fetch('./output/top10EconomicalBowlersIn2015.json')
    .then((response) => response.json())
    .then((data) => generateGraph4(data))
function generateGraph4(top10EconomicalBowlersIn2015) {
    Highcharts.chart('container4', {
        chart: {
          type: 'bar'
        },
        title: {
          text: 'Top 10 Economical Bowlers In 2015 '
        },
        subtitle: {
          text: 'IPL'
        },
        xAxis: {
          categories: Object.keys(top10EconomicalBowlersIn2015)
        },
        yAxis: {
          min: 0,
          title: {
            text: 'economy rate',
            align: 'high'
          },
          labels: {
            overflow: 'justify'
          }
        },
        
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -100,
          y: 10,
          floating: true,
          borderWidth: 1,
          backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFF1F',
          shadow: true
        },
        plotOptions: {
            bar: {
              dataLabels: {
                enabled: true
              }
            }
        },
        series: [{
          name: 'Year 2015',
          data: Object.values(top10EconomicalBowlersIn2015)
        }]
      });
}
